import express, { json } from "express";
import { model, Schema } from "mongoose";
import { port } from "./src/config/config.js";
import connectDb from "./src/connectDb/connectDb.js";
import { bookRouter } from "./src/router/bookRouter.js";
import { firstRouter } from "./src/router/firstRouter.js";
import { foodRouter } from "./src/router/foodRouter.js";
import playerRouter from "./src/router/playerRouter.js";
import studentRouter from "./src/router/studentRouter.js";


let expressApp = express();
expressApp.use(json()) 
expressApp.use("/", firstRouter) 
expressApp.use("/food", foodRouter)
expressApp.use("/book", bookRouter)
expressApp.use("/students", studentRouter)
expressApp.use("/players", playerRouter)

connectDb()




// let studentData = {
//     name: "",
//     roll: 425,
//     address:"gagalphedi",
// }
// // try {
// //     await Student.create(studentData)
// // } catch (err) {
// //     let error= new Error(err.message)
// //     throw error
// // }
// // try {
// //     let result = await Student.find({})
// //     console.log(result)
// // } catch (err) {
// //     let error= new Error(err.message)
// //     throw error
// // }

// let updateData = {
//     name: "ram",
//     roll: 425,
//     address:"gagalphedi",
// }

// try {
//        await Student.findByIdAndUpdate("6411ac736bcabfdaff56da95",updateData)
      
//     } catch (err) {
//         let error= new Error(err.message)
//         throw error
//     }
// try {
//       await Student.findByIdAndDelete("6411ac736bcabfdaff56da95")
       
//     } catch (err) {
//         let error= new Error(err.message)
//         throw error
//     }


expressApp.listen(port, () => {
    console.log(`express app is listening at port ${port}`);
});

