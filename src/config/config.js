import { config } from "dotenv";
config();
export let port = process.env.PORT
export let dbUrl = process.env.DBURL|| "mongodb://127.0.0.1:27017/express-dw2";