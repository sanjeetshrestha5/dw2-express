import { Player } from "../schema/model.js"

export let createPlayer = async (req, res) => {
    try {
        let result = await Player.create(req.body)
        
        res.json({
            success: true,
            result,
        })
    } catch (error) {
        let err = new Error(error.message)
        throw err
    }
}
export let readAllPlayer = async (req, res) => {
    try {
        let result = await Player.find({})
        
        res.json({
            success: true,
            result,
        })
    } catch (error) {
        let err = new Error(error.message)
        throw err
    }
}
export let readAllPlayerDetails = async (req, res) => {
    try {
        let result = await Player.findById(req.params.id)
        
        res.json({
            success: true,
            result,
        })
    } catch (error) {
        let err = new Error(error.message)
        throw err
    }
}
export let updatePlayerDetails = async (req, res) => {
    try {
        let result = await Player.findById(req.params.id,req.body,{new:true})
        
        res.json({
            success: true,
            result,
        })
    } catch (error) {
        let err = new Error(error.message)
        throw err
    }
}
export let deletePlayer= async (req, res) => {
    try {
        let result = await Player.findById(req.params.id)
        
        res.json({
            success: true,
            result,
        })
    } catch (error) {
        let err = new Error(error.message)
        throw err
    }
}