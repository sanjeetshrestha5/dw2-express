import { Router } from "express";


export const firstRouter = Router()

firstRouter
    .route("/")
    .post((req, res) => {
    res.json(" i am post")
    })
    .get((req, res) => {
    res.json("i am get")
    })
    .patch((req,res) => {
    res.json("i am patch")
    })
    .delete((req, res) => {
    res.json("i am delete")
})
firstRouter
    .route("/name")
    .post((req, res) => {
    res.json(" i am post name")
    })
    .get((req, res) => {
    res.json("i am get name")
    })
    .patch((req,res) => {
    res.json("i am patch name")
    })
    .delete((req, res) => {
    res.json("i am delete name")
})