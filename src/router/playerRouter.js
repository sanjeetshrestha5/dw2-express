import { Router } from "express";
import {
  createPlayer,
  deletePlayer,
  readAllPlayer,
  readAllPlayerDetails,
  updatePlayerDetails,
} from "../controller/playerController.js";
import validation from "../middleware/validation.js";
import playerValidation from "../validations/playerValidation.js";

const playerRouter = Router();

playerRouter
  .route("/")
  .post(validation(playerValidation), createPlayer)
  .get(readAllPlayer);

playerRouter
  .route("/:id")
  .get(readAllPlayerDetails)
  .patch(validation(playerValidation), updatePlayerDetails)
  .delete(deletePlayer);
export default playerRouter;
