import { Router } from "express";
import {
  createStudent,
  deleteStudent,
  readAllStudent,
  readAllStudentDetails,
  updateStudentDetails,
} from "../controller/studentController.js";
import validation from "../middleware/validation.js";
import studentValidation from "../validations/studentValidation.js";

const studentRouter = Router();

studentRouter
  .route("/")
  .post(validation(studentValidation), createStudent)
  .get(readAllStudent);

studentRouter
  .route("/:id")
  .get(readAllStudentDetails)
  .patch(validation(studentValidation), updateStudentDetails)
  .delete(deleteStudent);
export default studentRouter;
