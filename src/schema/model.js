import { model } from "mongoose";
import playerSchema from "./playerSchema.js";
import studentSchema from "./studentSchema.js";

export let Student = model("student", studentSchema)
export let Player = model("player", playerSchema)
