import { Schema } from "mongoose";
let playerGameSchema = Schema({
  matchName: {
    type: String,
  },
  noOfGoal: {
    type: String,
  },
});

let parentInfoSchema = Schema({
  fatherName: {
    type: String,
  },
  motherName: {
    type: String,
  },
});

let playerSchema = Schema(
  {
    name: {
      type: String,
    },
    age: {
      type: Number,
    },
    noOfMatches: {
      type: Number,
    },
    isMarried: {
      type: Boolean,
    },
    spouse: {
      type: String,
    },
    gender: {
      type: String,
      default: "male",
    },
    parentInfo: parentInfoSchema,
    playerGame: [playerGameSchema],
    favFood: [{ type: String }],
  },
  {
    timestamps: true,
  }
);
export default playerSchema;
