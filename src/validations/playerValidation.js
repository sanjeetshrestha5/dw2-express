import joi from "joi";
const playerValidation = joi.object().keys({
  name: joi.string().required(),
  age: joi.number().required(),
  noOfMatches: joi.number().required(),
});
export default playerValidation;
